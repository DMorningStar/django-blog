from django.shortcuts import render, HttpResponse
from django.http.response import HttpResponseRedirect, HttpResponseBadRequest, HttpResponseNotAllowed, \
    HttpResponseNotFound
from django.views.generic import ListView
from django.urls import reverse

from myblog.models import Post, Comment
from django.core.mail import send_mail
from datetime import datetime

def about(request):
    return render(request, 'blog/about.html')


def contact(request):
    return render(request, 'blog/contact.html')


def contact_me(request):
    if request.method == 'POST':
        send_mail(
            'Contact Form request from ' + request.POST['name']
            + ' with phone = ' + request.POST['phone'],
            request.POST['message'],
            request.POST['email'],
            ['maksutov.dmitry@gmail.com'],
            fail_silently=True,
        )
        return HttpResponse(status=204)
    else:
        return HttpResponseNotAllowed(permitted_methods='POST')


class PostListView(ListView):
    model = Post
    paginate_by = 3
    template_name = 'blog/blog.html'
    ordering = '-date'


def add_comment(request, pk):
    if request.method == 'POST':
        try:
            current_post = Post.objects.get(pk=pk)
            Comment.objects.create(author=request.POST['author'],
                                   message=request.POST['comment'],
                                   post=current_post, comment_date=datetime.now()).save()
            return HttpResponseRedirect(reverse('post', kwargs={'pk': current_post.pk}))
        except Post.DoesNotExist:
            return HttpResponseBadRequest
    else:
        return HttpResponseNotAllowed(permitted_methods='POST')


def post(request, pk):
    if request.method == 'GET':
        try:
            current_post = Post.objects.get(pk=pk)
            return render(request, 'blog/post.html', {'post': current_post,
                                               'comments': Comment.objects.filter(post=current_post)})
        except Post.DoesNotExist:
            return HttpResponseNotFound()
    else:
        return HttpResponseNotAllowed(permitted_methods='GET')
