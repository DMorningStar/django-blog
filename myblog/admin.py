from django.contrib import admin
from myblog.models import Post
from quill.admin import QuillAdmin
from quill.apps import QuillConfig


class MyQuillConfig(QuillConfig):
    allowed_image_extensions = ['jpeg', 'gif']

admin.site.register(Post, QuillAdmin)
