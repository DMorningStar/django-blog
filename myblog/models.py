from django.db import models
from quill.fields import RichTextField
import os


def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)


class Post(models.Model):
    title = models.CharField(max_length=140)
    post_image = models.ImageField(upload_to=get_image_path, blank=True, null=True)
    content = RichTextField()
    preview_content = RichTextField(config='basic')
    date = models.DateTimeField()

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    author = models.CharField(max_length=140)
    message = models.TextField()
    comment_date = models.DateTimeField()

    def __str__(self):
        return self.message
