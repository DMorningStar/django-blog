from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [
    url(r'^$', RedirectView.as_view(url='/blog')),
    url(r'^blog$', views.PostListView.as_view(), name='post_list'),
    url(r'^posts/(?P<pk>\d+)$', views.post, name='post'),
    url(r'^posts/(?P<pk>\d+)/comments$', views.add_comment),
    url(r'^contact/', views.contact, name='contact'),
    url(r'^about/', views.about, name='contact'),
    url(r'^mail/contact_me', views.contact_me, name='contact_me'),
]
