from django import template
from datetime import datetime

register = template.Library()


@register.filter(name='days_from_now')
def days_from_now(value):
    return (datetime.now() - value.replace(tzinfo=None)).days
